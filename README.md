# Forager Example Mod

A tiny mod which showcases all of the Forager Modding API's features.

This mod is written and maintained by lazyeye (Gabe Weiner). You are free to copy and use as you need!

A full guide on Forager modding can be found in our documentation: https://playforager.com